﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOTJAR_programmering_opgaver
{
    class Program
    {
        static void Main(string[] args)
        {
            Opgave2();
            Console.ReadLine();
        }
        static void Opgave1() { 
        string[] array = new string[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j" };
        Console.WriteLine("Elements in array are: ");
            for (int i = 0; i<array.Length; i++)
            {
                Console.WriteLine(" {0} {1}", i, array[i]);
            }

        }
        static void Opgave2()
        {
            int[] array = new int[] { 2, 5, 7, 6, 4, 6 };
            Console.Write("The values store into the array are: ");
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write(array[i] + " ");
            }
            Console.Write("\nThe values store into the array in reverse are: ");
            for (int i = array.Length - 1; i >= 0; i--)
            {
                Console.Write(array[i] + " ");
            }

        }
    }
    
}
